package alin.harasim.prim;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Aplicatie {

	public static void main(String[] args) throws IOException {
		try {
		System.out.print ("Introduceti capatul n1 al intervalului: ");
		BufferedReader b1 = new BufferedReader (new InputStreamReader (System.in));
		String n1_str = b1.readLine ();
		int n1 = Integer.parseInt(n1_str);
		System.out.print ("Introduceti capatul n2 al intervalului: ");
		String n2_str = b1.readLine ();
		int n2 = Integer.parseInt(n2_str);
		
		if(n1<n2) {
		int i;
		for(i=n1; i<=n2; i++)
		{
			int divizori=0,d=1;
			
			while (d <= i) {
				if (i % d == 0) {
					divizori++;
		                	if (divizori == 3) {
		                        	break;
		                	}
		                }
				d++;
			}
			if (divizori == 2) {
				System.out.println(i + " este numar prim , deci in intervalul [" + n1 + "," + n2 + "] exista cel putin un numar prim");
			} 
		}
			}else System.out.println("Eroare, n1 este mai mare sau egal ca n2!");
		}catch(NumberFormatException ne) {System.out.println(ne.getMessage() + " nu este valid"); }
	}
}
